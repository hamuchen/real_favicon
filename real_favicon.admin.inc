<?php

function real_favicon_admin() {
  $form = array();

  $form['favicon'] = array(
    '#type' => 'fieldset',
    '#title' => 'Real Favicon',
  );
  $form['favicon']['real_favicon_html_head'] = array(
    '#type' => 'textarea',
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('real_favicon_html_head', ''),
    '#description' => t('Paste the code from your generated favicon here.'),
  );

  return system_settings_form($form);
}
